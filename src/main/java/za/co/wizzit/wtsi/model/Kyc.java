package za.co.wizzit.wtsi.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
public class Kyc {
    private Integer id;
    private Integer group;
    private String name;
    private Integer level;
    private String updated;
    private String createdBy;
    private String updatedBy;
    private Timestamp created;
    private Timestamp updatedCopy1;
    private String status;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "group")
    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "level")
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Basic
    @Column(name = "updated")
    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated_copy1")
    public Timestamp getUpdatedCopy1() {
        return updatedCopy1;
    }

    public void setUpdatedCopy1(Timestamp updatedCopy1) {
        this.updatedCopy1 = updatedCopy1;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kyc kyc = (Kyc) o;

        if (id != null ? !id.equals(kyc.id) : kyc.id != null) return false;
        if (group != null ? !group.equals(kyc.group) : kyc.group != null) return false;
        if (name != null ? !name.equals(kyc.name) : kyc.name != null) return false;
        if (level != null ? !level.equals(kyc.level) : kyc.level != null) return false;
        if (updated != null ? !updated.equals(kyc.updated) : kyc.updated != null) return false;
        if (createdBy != null ? !createdBy.equals(kyc.createdBy) : kyc.createdBy != null) return false;
        if (updatedBy != null ? !updatedBy.equals(kyc.updatedBy) : kyc.updatedBy != null) return false;
        if (created != null ? !created.equals(kyc.created) : kyc.created != null) return false;
        if (updatedCopy1 != null ? !updatedCopy1.equals(kyc.updatedCopy1) : kyc.updatedCopy1 != null) return false;
        if (status != null ? !status.equals(kyc.status) : kyc.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updatedCopy1 != null ? updatedCopy1.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
