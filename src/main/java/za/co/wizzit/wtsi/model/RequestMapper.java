package za.co.wizzit.wtsi.model;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
@IdClass(RequestMapperPK.class)
public class RequestMapper {
    private Integer id;
    private String inParameter;
    private String outParameter;
    private Byte required;
    private byte[] defaultValue;
    private String transactionTypeCode;
    private String stepName;
    private String created;
    private String updated;
    private String createdBy;
    private String updatedBy;
    private String status;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "inParameter")
    public String getInParameter() {
        return inParameter;
    }

    public void setInParameter(String inParameter) {
        this.inParameter = inParameter;
    }

    @Basic
    @Column(name = "outParameter")
    public String getOutParameter() {
        return outParameter;
    }

    public void setOutParameter(String outParameter) {
        this.outParameter = outParameter;
    }

    @Basic
    @Column(name = "required")
    public Byte getRequired() {
        return required;
    }

    public void setRequired(Byte required) {
        this.required = required;
    }

    @Basic
    @Column(name = "defaultValue")
    public byte[] getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(byte[] defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Id
    @Column(name = "TransactionTypeCode")
    public String getTransactionTypeCode() {
        return transactionTypeCode;
    }

    public void setTransactionTypeCode(String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }

    @Basic
    @Column(name = "stepName")
    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    @Basic
    @Column(name = "created")
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated")
    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequestMapper that = (RequestMapper) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (inParameter != null ? !inParameter.equals(that.inParameter) : that.inParameter != null) return false;
        if (outParameter != null ? !outParameter.equals(that.outParameter) : that.outParameter != null) return false;
        if (required != null ? !required.equals(that.required) : that.required != null) return false;
        if (!Arrays.equals(defaultValue, that.defaultValue)) return false;
        if (transactionTypeCode != null ? !transactionTypeCode.equals(that.transactionTypeCode) : that.transactionTypeCode != null)
            return false;
        if (stepName != null ? !stepName.equals(that.stepName) : that.stepName != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (inParameter != null ? inParameter.hashCode() : 0);
        result = 31 * result + (outParameter != null ? outParameter.hashCode() : 0);
        result = 31 * result + (required != null ? required.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(defaultValue);
        result = 31 * result + (transactionTypeCode != null ? transactionTypeCode.hashCode() : 0);
        result = 31 * result + (stepName != null ? stepName.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
