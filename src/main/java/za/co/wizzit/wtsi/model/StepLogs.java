package za.co.wizzit.wtsi.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
@IdClass(StepLogsPK.class)
public class StepLogs {
    private Integer id;
    private Timestamp created;
    private Timestamp updated;
    private String createdBy;
    private String updatedBy;
    private String status;
    private String name;
    private byte[] request;
    private byte[] response;
    private String transactionsEverestRef;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "request")
    public byte[] getRequest() {
        return request;
    }

    public void setRequest(byte[] request) {
        this.request = request;
    }

    @Basic
    @Column(name = "response")
    public byte[] getResponse() {
        return response;
    }

    public void setResponse(byte[] response) {
        this.response = response;
    }

    @Id
    @Column(name = "Transactions_everest_ref")
    public String getTransactionsEverestRef() {
        return transactionsEverestRef;
    }

    public void setTransactionsEverestRef(String transactionsEverestRef) {
        this.transactionsEverestRef = transactionsEverestRef;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StepLogs stepLogs = (StepLogs) o;

        if (id != null ? !id.equals(stepLogs.id) : stepLogs.id != null) return false;
        if (created != null ? !created.equals(stepLogs.created) : stepLogs.created != null) return false;
        if (updated != null ? !updated.equals(stepLogs.updated) : stepLogs.updated != null) return false;
        if (createdBy != null ? !createdBy.equals(stepLogs.createdBy) : stepLogs.createdBy != null) return false;
        if (updatedBy != null ? !updatedBy.equals(stepLogs.updatedBy) : stepLogs.updatedBy != null) return false;
        if (status != null ? !status.equals(stepLogs.status) : stepLogs.status != null) return false;
        if (name != null ? !name.equals(stepLogs.name) : stepLogs.name != null) return false;
        if (!Arrays.equals(request, stepLogs.request)) return false;
        if (!Arrays.equals(response, stepLogs.response)) return false;
        if (transactionsEverestRef != null ? !transactionsEverestRef.equals(stepLogs.transactionsEverestRef) : stepLogs.transactionsEverestRef != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(request);
        result = 31 * result + Arrays.hashCode(response);
        result = 31 * result + (transactionsEverestRef != null ? transactionsEverestRef.hashCode() : 0);
        return result;
    }
}
