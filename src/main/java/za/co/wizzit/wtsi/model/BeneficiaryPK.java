package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by johanvorster on 2/9/18.
 */
public class BeneficiaryPK implements Serializable {
    private Integer id;
    private Integer beneficiaryGroup;
    private String transactionType;

    @Column(name = "id")
    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "beneficiaryGroup")
    @Id
    public Integer getBeneficiaryGroup() {
        return beneficiaryGroup;
    }

    public void setBeneficiaryGroup(Integer beneficiaryGroup) {
        this.beneficiaryGroup = beneficiaryGroup;
    }

    @Column(name = "transactionType")
    @Id
    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BeneficiaryPK that = (BeneficiaryPK) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (beneficiaryGroup != null ? !beneficiaryGroup.equals(that.beneficiaryGroup) : that.beneficiaryGroup != null)
            return false;
        if (transactionType != null ? !transactionType.equals(that.transactionType) : that.transactionType != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (beneficiaryGroup != null ? beneficiaryGroup.hashCode() : 0);
        result = 31 * result + (transactionType != null ? transactionType.hashCode() : 0);
        return result;
    }
}
