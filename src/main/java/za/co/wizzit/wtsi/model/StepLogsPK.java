package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by johanvorster on 2/9/18.
 */
public class StepLogsPK implements Serializable {
    private Integer id;
    private String transactionsEverestRef;

    @Column(name = "id")
    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Transactions_everest_ref")
    @Id
    public String getTransactionsEverestRef() {
        return transactionsEverestRef;
    }

    public void setTransactionsEverestRef(String transactionsEverestRef) {
        this.transactionsEverestRef = transactionsEverestRef;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StepLogsPK that = (StepLogsPK) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (transactionsEverestRef != null ? !transactionsEverestRef.equals(that.transactionsEverestRef) : that.transactionsEverestRef != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (transactionsEverestRef != null ? transactionsEverestRef.hashCode() : 0);
        return result;
    }
}
