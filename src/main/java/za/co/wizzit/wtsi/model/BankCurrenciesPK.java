package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by johanvorster on 2/9/18.
 */
public class BankCurrenciesPK implements Serializable {
    private String currency;
    private Integer bank;

    @Column(name = "currency")
    @Id
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Column(name = "bank")
    @Id
    public Integer getBank() {
        return bank;
    }

    public void setBank(Integer bank) {
        this.bank = bank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankCurrenciesPK that = (BankCurrenciesPK) o;

        if (currency != null ? !currency.equals(that.currency) : that.currency != null) return false;
        if (bank != null ? !bank.equals(that.bank) : that.bank != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = currency != null ? currency.hashCode() : 0;
        result = 31 * result + (bank != null ? bank.hashCode() : 0);
        return result;
    }
}
