package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by johanvorster on 2/9/18.
 */
public class MessagePK implements Serializable {
    private String code;
    private String langauge;

    @Column(name = "code")
    @Id
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "langauge")
    @Id
    public String getLangauge() {
        return langauge;
    }

    public void setLangauge(String langauge) {
        this.langauge = langauge;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessagePK messagePK = (MessagePK) o;

        if (code != null ? !code.equals(messagePK.code) : messagePK.code != null) return false;
        if (langauge != null ? !langauge.equals(messagePK.langauge) : messagePK.langauge != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (langauge != null ? langauge.hashCode() : 0);
        return result;
    }
}
