package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
@IdClass(AccountTypeTransactionTypePK.class)
public class AccountTypeTransactionType {
    private String accountType;
    private String transactionType;

    @Id
    @Column(name = "accountType")
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @Id
    @Column(name = "transactionType")
    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountTypeTransactionType that = (AccountTypeTransactionType) o;

        if (accountType != null ? !accountType.equals(that.accountType) : that.accountType != null) return false;
        if (transactionType != null ? !transactionType.equals(that.transactionType) : that.transactionType != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = accountType != null ? accountType.hashCode() : 0;
        result = 31 * result + (transactionType != null ? transactionType.hashCode() : 0);
        return result;
    }
}
