package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by johanvorster on 2/9/18.
 */
public class FlowStepPK implements Serializable {
    private String flow;
    private Integer step;

    @Column(name = "flow")
    @Id
    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    @Column(name = "step")
    @Id
    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlowStepPK that = (FlowStepPK) o;

        if (flow != null ? !flow.equals(that.flow) : that.flow != null) return false;
        if (step != null ? !step.equals(that.step) : that.step != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = flow != null ? flow.hashCode() : 0;
        result = 31 * result + (step != null ? step.hashCode() : 0);
        return result;
    }
}
