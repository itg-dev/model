package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by johanvorster on 2/9/18.
 */
public class AccountTypeTransactionTypePK implements Serializable {
    private String accountType;
    private String transactionType;

    @Column(name = "accountType")
    @Id
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @Column(name = "transactionType")
    @Id
    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountTypeTransactionTypePK that = (AccountTypeTransactionTypePK) o;

        if (accountType != null ? !accountType.equals(that.accountType) : that.accountType != null) return false;
        if (transactionType != null ? !transactionType.equals(that.transactionType) : that.transactionType != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = accountType != null ? accountType.hashCode() : 0;
        result = 31 * result + (transactionType != null ? transactionType.hashCode() : 0);
        return result;
    }
}
