package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by johanvorster on 2/9/18.
 */
public class KycCustomFieldsPK implements Serializable {
    private Integer kyc;
    private Integer customField;

    @Column(name = "kyc")
    @Id
    public Integer getKyc() {
        return kyc;
    }

    public void setKyc(Integer kyc) {
        this.kyc = kyc;
    }

    @Column(name = "customField")
    @Id
    public Integer getCustomField() {
        return customField;
    }

    public void setCustomField(Integer customField) {
        this.customField = customField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KycCustomFieldsPK that = (KycCustomFieldsPK) o;

        if (kyc != null ? !kyc.equals(that.kyc) : that.kyc != null) return false;
        if (customField != null ? !customField.equals(that.customField) : that.customField != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kyc != null ? kyc.hashCode() : 0;
        result = 31 * result + (customField != null ? customField.hashCode() : 0);
        return result;
    }
}
