package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
@IdClass(KycCustomFieldsPK.class)
public class KycCustomFields {
    private Integer kyc;
    private Integer customField;

    @Id
    @Column(name = "kyc")
    public Integer getKyc() {
        return kyc;
    }

    public void setKyc(Integer kyc) {
        this.kyc = kyc;
    }

    @Id
    @Column(name = "customField")
    public Integer getCustomField() {
        return customField;
    }

    public void setCustomField(Integer customField) {
        this.customField = customField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KycCustomFields that = (KycCustomFields) o;

        if (kyc != null ? !kyc.equals(that.kyc) : that.kyc != null) return false;
        if (customField != null ? !customField.equals(that.customField) : that.customField != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kyc != null ? kyc.hashCode() : 0;
        result = 31 * result + (customField != null ? customField.hashCode() : 0);
        return result;
    }
}
