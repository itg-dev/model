package za.co.wizzit.wtsi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
@IdClass(AccounTransactionLimitPK.class)
public class AccounTransactionLimit {
    private String account;
    private Integer transactionLimit;
    private String transactionType;

    @Id
    @Column(name = "account")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Id
    @Column(name = "transactionLimit")
    public Integer getTransactionLimit() {
        return transactionLimit;
    }

    public void setTransactionLimit(Integer transactionLimit) {
        this.transactionLimit = transactionLimit;
    }

    @Id
    @Column(name = "transactionType")
    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccounTransactionLimit that = (AccounTransactionLimit) o;

        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        if (transactionLimit != null ? !transactionLimit.equals(that.transactionLimit) : that.transactionLimit != null)
            return false;
        if (transactionType != null ? !transactionType.equals(that.transactionType) : that.transactionType != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = account != null ? account.hashCode() : 0;
        result = 31 * result + (transactionLimit != null ? transactionLimit.hashCode() : 0);
        result = 31 * result + (transactionType != null ? transactionType.hashCode() : 0);
        return result;
    }
}
