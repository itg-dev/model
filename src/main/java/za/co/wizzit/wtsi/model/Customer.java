package za.co.wizzit.wtsi.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
public class Customer {
    private Integer id;
    private String createdBy;
    private String updatedBy;
    private String status;
    private Integer customerDetail;
    private String customerType;
    private String defaultAccount;
    private Timestamp created;
    private Timestamp updated;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "customerDetail")
    public Integer getCustomerDetail() {
        return customerDetail;
    }

    public void setCustomerDetail(Integer customerDetail) {
        this.customerDetail = customerDetail;
    }

    @Basic
    @Column(name = "customerType")
    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    @Basic
    @Column(name = "defaultAccount")
    public String getDefaultAccount() {
        return defaultAccount;
    }

    public void setDefaultAccount(String defaultAccount) {
        this.defaultAccount = defaultAccount;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (id != null ? !id.equals(customer.id) : customer.id != null) return false;
        if (createdBy != null ? !createdBy.equals(customer.createdBy) : customer.createdBy != null) return false;
        if (updatedBy != null ? !updatedBy.equals(customer.updatedBy) : customer.updatedBy != null) return false;
        if (status != null ? !status.equals(customer.status) : customer.status != null) return false;
        if (customerDetail != null ? !customerDetail.equals(customer.customerDetail) : customer.customerDetail != null)
            return false;
        if (customerType != null ? !customerType.equals(customer.customerType) : customer.customerType != null)
            return false;
        if (defaultAccount != null ? !defaultAccount.equals(customer.defaultAccount) : customer.defaultAccount != null)
            return false;
        if (created != null ? !created.equals(customer.created) : customer.created != null) return false;
        if (updated != null ? !updated.equals(customer.updated) : customer.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (customerDetail != null ? customerDetail.hashCode() : 0);
        result = 31 * result + (customerType != null ? customerType.hashCode() : 0);
        result = 31 * result + (defaultAccount != null ? defaultAccount.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }
}
