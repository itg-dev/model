package za.co.wizzit.wtsi.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
public class Transactions {
    private String everestRef;
    private Serializable status;
    private Timestamp startTime;
    private Timestamp completionTime;
    private String account;
    private String user;
    private String application;

    @Id
    @Column(name = "everest_ref")
    public String getEverestRef() {
        return everestRef;
    }

    public void setEverestRef(String everestRef) {
        this.everestRef = everestRef;
    }

    @Basic
    @Column(name = "status")
    public Serializable getStatus() {
        return status;
    }

    public void setStatus(Serializable status) {
        this.status = status;
    }

    @Basic
    @Column(name = "startTime")
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "completionTime")
    public Timestamp getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(Timestamp completionTime) {
        this.completionTime = completionTime;
    }

    @Basic
    @Column(name = "account")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Basic
    @Column(name = "user")
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Basic
    @Column(name = "application")
    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transactions that = (Transactions) o;

        if (everestRef != null ? !everestRef.equals(that.everestRef) : that.everestRef != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
        if (completionTime != null ? !completionTime.equals(that.completionTime) : that.completionTime != null)
            return false;
        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        if (application != null ? !application.equals(that.application) : that.application != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = everestRef != null ? everestRef.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (completionTime != null ? completionTime.hashCode() : 0);
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (application != null ? application.hashCode() : 0);
        return result;
    }
}
