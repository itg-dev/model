package za.co.wizzit.wtsi.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
public class Change {
    private Integer id;
    private Timestamp created;
    private Timestamp updated;
    private String createdBy;
    private String updatedBy;
    private String status;
    private byte[] currentDetail;
    private byte[] previousDetail;
    private Serializable changeType;
    private String model;
    private String modelId;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "currentDetail")
    public byte[] getCurrentDetail() {
        return currentDetail;
    }

    public void setCurrentDetail(byte[] currentDetail) {
        this.currentDetail = currentDetail;
    }

    @Basic
    @Column(name = "previousDetail")
    public byte[] getPreviousDetail() {
        return previousDetail;
    }

    public void setPreviousDetail(byte[] previousDetail) {
        this.previousDetail = previousDetail;
    }

    @Basic
    @Column(name = "changeType")
    public Serializable getChangeType() {
        return changeType;
    }

    public void setChangeType(Serializable changeType) {
        this.changeType = changeType;
    }

    @Basic
    @Column(name = "model")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Basic
    @Column(name = "modelID")
    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Change change = (Change) o;

        if (id != null ? !id.equals(change.id) : change.id != null) return false;
        if (created != null ? !created.equals(change.created) : change.created != null) return false;
        if (updated != null ? !updated.equals(change.updated) : change.updated != null) return false;
        if (createdBy != null ? !createdBy.equals(change.createdBy) : change.createdBy != null) return false;
        if (updatedBy != null ? !updatedBy.equals(change.updatedBy) : change.updatedBy != null) return false;
        if (status != null ? !status.equals(change.status) : change.status != null) return false;
        if (!Arrays.equals(currentDetail, change.currentDetail)) return false;
        if (!Arrays.equals(previousDetail, change.previousDetail)) return false;
        if (changeType != null ? !changeType.equals(change.changeType) : change.changeType != null) return false;
        if (model != null ? !model.equals(change.model) : change.model != null) return false;
        if (modelId != null ? !modelId.equals(change.modelId) : change.modelId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(currentDetail);
        result = 31 * result + Arrays.hashCode(previousDetail);
        result = 31 * result + (changeType != null ? changeType.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (modelId != null ? modelId.hashCode() : 0);
        return result;
    }
}
