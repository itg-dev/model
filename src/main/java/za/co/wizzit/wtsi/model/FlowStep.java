package za.co.wizzit.wtsi.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
@IdClass(FlowStepPK.class)
public class FlowStep {
    private String flow;
    private Integer step;
    private Integer condition;
    private Integer transactionTemplate;
    private Integer notificationTemplate;
    private Timestamp created;
    private Timestamp updated;
    private String createdBy;
    private String updatedBy;
    private String status;

    @Id
    @Column(name = "flow")
    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    @Id
    @Column(name = "step")
    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    @Basic
    @Column(name = "condition")
    public Integer getCondition() {
        return condition;
    }

    public void setCondition(Integer condition) {
        this.condition = condition;
    }

    @Basic
    @Column(name = "transactionTemplate")
    public Integer getTransactionTemplate() {
        return transactionTemplate;
    }

    public void setTransactionTemplate(Integer transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    @Basic
    @Column(name = "notificationTemplate")
    public Integer getNotificationTemplate() {
        return notificationTemplate;
    }

    public void setNotificationTemplate(Integer notificationTemplate) {
        this.notificationTemplate = notificationTemplate;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlowStep flowStep = (FlowStep) o;

        if (flow != null ? !flow.equals(flowStep.flow) : flowStep.flow != null) return false;
        if (step != null ? !step.equals(flowStep.step) : flowStep.step != null) return false;
        if (condition != null ? !condition.equals(flowStep.condition) : flowStep.condition != null) return false;
        if (transactionTemplate != null ? !transactionTemplate.equals(flowStep.transactionTemplate) : flowStep.transactionTemplate != null)
            return false;
        if (notificationTemplate != null ? !notificationTemplate.equals(flowStep.notificationTemplate) : flowStep.notificationTemplate != null)
            return false;
        if (created != null ? !created.equals(flowStep.created) : flowStep.created != null) return false;
        if (updated != null ? !updated.equals(flowStep.updated) : flowStep.updated != null) return false;
        if (createdBy != null ? !createdBy.equals(flowStep.createdBy) : flowStep.createdBy != null) return false;
        if (updatedBy != null ? !updatedBy.equals(flowStep.updatedBy) : flowStep.updatedBy != null) return false;
        if (status != null ? !status.equals(flowStep.status) : flowStep.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = flow != null ? flow.hashCode() : 0;
        result = 31 * result + (step != null ? step.hashCode() : 0);
        result = 31 * result + (condition != null ? condition.hashCode() : 0);
        result = 31 * result + (transactionTemplate != null ? transactionTemplate.hashCode() : 0);
        result = 31 * result + (notificationTemplate != null ? notificationTemplate.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
