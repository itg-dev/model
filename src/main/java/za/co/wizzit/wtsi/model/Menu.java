package za.co.wizzit.wtsi.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Created by johanvorster on 2/9/18.
 */
@Entity
public class Menu {
    private Integer id;
    private String version;
    private byte[] menu;
    private String status;
    private Timestamp created;
    private Timestamp updated;
    private String createdBy;
    private String updatedBy;
    private String application;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "version")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Basic
    @Column(name = "menu")
    public byte[] getMenu() {
        return menu;
    }

    public void setMenu(byte[] menu) {
        this.menu = menu;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "created")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated")
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "application")
    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Menu menu1 = (Menu) o;

        if (id != null ? !id.equals(menu1.id) : menu1.id != null) return false;
        if (version != null ? !version.equals(menu1.version) : menu1.version != null) return false;
        if (!Arrays.equals(menu, menu1.menu)) return false;
        if (status != null ? !status.equals(menu1.status) : menu1.status != null) return false;
        if (created != null ? !created.equals(menu1.created) : menu1.created != null) return false;
        if (updated != null ? !updated.equals(menu1.updated) : menu1.updated != null) return false;
        if (createdBy != null ? !createdBy.equals(menu1.createdBy) : menu1.createdBy != null) return false;
        if (updatedBy != null ? !updatedBy.equals(menu1.updatedBy) : menu1.updatedBy != null) return false;
        if (application != null ? !application.equals(menu1.application) : menu1.application != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(menu);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        result = 31 * result + (application != null ? application.hashCode() : 0);
        return result;
    }
}
